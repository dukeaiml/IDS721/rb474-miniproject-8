# rb474-miniproject-8

## Task
Rust Command-Line Tool with Testing

Requirements
- Rust command-line tool
- Data ingestion/processing
- Unit tests

## Description
The program is capitalizes all the strings provided as arguments and prints them each on a new line. At least one arguments must be provided.

## Usage
To test the functionality of the program, run:

```bash
cargo build
cargo run [your strings]
```
### Example of running the program
![](images/sample_output.jpeg)

## Test
To run the tests:

```bash
cargo test
```

### Output of running the tests
![](/images/test_log.jpeg)
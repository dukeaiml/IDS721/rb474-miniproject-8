use std::env;

fn main() {
	let args: Vec<String> = env::args().collect();

	if args.len() < 2 {
		eprintln!("At least one argument must be provided");
		std::process::exit(1);
	}

	for arg in args.iter().skip(1) {
		println!("{}", capitalize_string(arg));
	}	
}

fn capitalize_string(input: &str) -> String {
	input.to_uppercase()
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_capitalize_string() {
		let input = "inPuT!";

		let result = capitalize_string(input);

		assert_eq!(result, "INPUT!");
	}
}
